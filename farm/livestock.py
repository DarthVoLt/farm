def fib(n):
    """Вычисления числа Фибоначчи."""
    a = 2
    b = 3
    for __ in range(n):
        a, b = b, a + b
    return a


def bunnies(day):
    """
    Подсчёт выводка кроликов.
    Кролики размножаются раз в 10 дней в соответствии с числами Фибоначчи.
    """
    how_many_times = day // 10
    return fib(how_many_times)


def cow(day):
    """
    Подсчёт удоя коровы
    Корова даёт молоко раз в 3 дня. В первый день месяца она даёт 500 мл, затем с каждым разом на 50 мл меньше. В начале
     нового месяца её удой восполняется снова до 500 мл.
    """
    times = day / 3

    if times.is_integer():
        if day == 0:
            milk = 500
        else:
            milk = 500 - (times * 50)
    else:
        milk = 0

    return milk
