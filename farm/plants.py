import random


def sowing(grains):
    """
    Случайное засевание поля.
    0 - ничего не выросло, 1 - урожайная ячейка.
    """
    field = []
    for _ in range(grains):
        i = random.randint(0, 1)
        field.append(i)
    return field
    # return [random.randint(0, 1) for _ in range(grains)]


def corn(day):
    """Подсчёт початков кукурузы."""
    grains = (day + 1) * 2
    harvest = sowing(grains)
    counter = 0
    for corns in harvest:
        if corns == 1:
            counter += 1
    return counter


def berry_bush(day):
    """
    Подсчет урожая с ягодных кустов
    Ягодный куст увеличивается в росте каждые 15 дней в 15 раз. Изначально с него можно собрать случайное количество
    ягод от 50 до 100. После прорастания этот диапазон увеличивается.
    """
    times = day // 15

    if times == 0:
        berry_min = 50
        berry_max = 100
    else:
        berry_min = 50 * 15 * times
        berry_max = 100 * 15 * times

    return random.randrange(berry_min, berry_max)
